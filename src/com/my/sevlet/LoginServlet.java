/**
* @(#)com.my.sevlet.LoginServlet.java
* 南京中兴软创科技有限责任公司
* @date 2013-10-17
*/
package com.my.sevlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.my.entity.User;
import com.my.filter.LoginFilter;

/** 
 * 
 * @author <a href="tong.hao@zte.com.cn">童浩</a>
 * @version 1.0 
 * @since 2013-10-17 下午5:07:07 
 */

public class LoginServlet extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public LoginServlet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	// /servlet/login?name=tom&pwd=a
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		HttpSession session = request.getSession();
		PrintWriter out = response.getWriter();
		
		String name = request.getParameter("name");
		String pwd = request.getParameter("pwd");
		if(!LoginFilter.initUsers.containsKey(name)) {
			out.write("username not found");
		} else {
			if(LoginFilter.initUsers.get(name).equals(pwd)) {
				User currentUser = new User(name, pwd);
				//session.setAttribute("loginUser", currentUser);
				//session.setAttribute("test", "abc");
				Cookie[] cs = request.getCookies();
				
				String cookieId = createId();
				Cookie c = new Cookie("loginUser", cookieId);
				LoginFilter.ids.put(cookieId, currentUser);
				c.setMaxAge(1000); //关闭浏览器后，cookie立即失效          
				//c.setHttpOnly(true);// tomcat7才支持
			    c.setPath("/TestWebServlet");// 对于多个项目要设置不同的path,否则cookie会在整个服务器下都可以使用
			    //c.setPath("/");// 对于多个项目要设置不同的path,否则cookie会在整个服务器下都可以使用
				response.addCookie(c);
				
				//response.setHeader("Set-Cookie", "loginUser=" + cookieId + ";Path=/;Max-Age=1000;HttpOnly");
				response.sendRedirect("/TestWebServlet/index.jsp");
			} else {
				out.write("password not correct");
			}
		}
	}
	
	public String createId() {
		return System.currentTimeMillis() + "";
	}
	
	public String encrypt(String name) {
		return name + "-";
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the POST method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
