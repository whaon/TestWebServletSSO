/**
* @(#)com.my.entity.User.java
* 南京中兴软创科技有限责任公司
* @date 2013-10-17
*/
package com.my.entity;

/** 
 * 
 * @author <a href="tong.hao@zte.com.cn">童浩</a>
 * @version 1.0 
 * @since 2013-10-17 下午5:21:29 
 */

public class User {
	private String name;
	private String pwd;
	public User() {}
	public User(String name, String pwd) {
		super();
		this.name = name;
		this.pwd = pwd;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
}
