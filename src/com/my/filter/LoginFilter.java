/**
* @(#)com.my.filter.LoginFilter.java
* 南京中兴软创科技有限责任公司
* @date 2013-10-17
*/
package com.my.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.my.entity.User;

/** 
 * 
 * @author <a href="tong.hao@zte.com.cn">童浩</a>
 * @version 1.0 
 * @since 2013-10-17 下午5:07:58 
 */

public class LoginFilter implements Filter {
	
	public static Map<String, String> initUsers = new HashMap<String, String>();
	
	public static Map<String, User> ids = new HashMap<String, User>();

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		response.setContentType("text/html;charset=UTF-8");
		HttpServletRequest req= (HttpServletRequest)request;
		HttpServletResponse res= (HttpServletResponse)response;
		PrintWriter writer = response.getWriter();
		// 登录拦截
		String url = req.getRequestURI();
		if(url.contains("login") || url.contains("None")) {
			chain.doFilter(request, response);
			return;
		}
		//HttpSession session = req.getSession();
		//User u = (User) session.getAttribute("loginUser");
		/*if(u == null) {
			writer.println("请登录");
			return;
		} else {
			chain.doFilter(request, response);
		}*/
		
		Cookie[] cs = req.getCookies();
		if(cs != null) {
			for(Cookie c: cs) {
				System.out.println("f:" + c.getName() + "---" + c.getValue());
				if(c.getName().equals("loginUser")) {
					if(ids.containsKey(c.getValue())) {
						// 定期清理,踢掉,还是阻止
						if(req.getHeader("Referer") != null && req.getHeader("Referer").contains("TestWebServlet2")) {
							//writer.println("true");
							//res.sendRedirect("http://www.baidu.com");
							writer.println("<script language='javascript' type='text/javascript'>top.location.href='http://localsite:8080/TestWebServlet2/setCookie.jsp';</script>");
						} else {
							System.out.println(((User)ids.get(c.getValue())).getName() + "已登录");
							chain.doFilter(request, response);
						}
						return;
					}
				}
			}
		}
		
		writer.println("请登录1");
		return;
		
	}
	
	

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		initUsers.put("admin", "a");
		initUsers.put("tom", "a");
	}

}
